//
//  SlideOutAnimationController.swift
//  StoreSearch
//
//  Created by mazodirty on 3/06/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class SlideOutAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        //from identifica la vista que se esta mostrando actualmente antes de que se realice la presentation de un nuevo viewController, tambien es la vista final al realizar un dismiss como el que se realizara
        if let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) {
            
            let containerView = transitionContext.containerView
            let duration = transitionDuration(using: transitionContext)
            
            UIView.animate(
                withDuration: duration,
                animations: {
                    fromView.center.y -= containerView.bounds.size.height
                    fromView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            },
                completion: { (finished) in
                    transitionContext.completeTransition(finished)
            }
            )
        }
        
    }
    
}






//
//  ViewController.swift
//  StoreSearch
//
//  Created by mazodirty on 6/1/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    //var searchResults = [SearchResult]()
    //var hasSearched = false
    //var hasRequested = false
    //var dataTask: URLSessionDataTask?
    let search = Search()
    var landscapeVC: LandscapeVC?
    weak var splitViewDetail: DetailVC?
    
    struct IDS {
        static let searchCell = "SearchResultCell"
        static let loadingCell = "LoadingCell"
        static let notFoundCell = "NotFoundCell"
        static let detailSegue = "DetailSegue"
        static let landscapeVC = "LandscapeVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("Search", comment: "Split-view master button")
        
        searchBar.delegate = self
        
        tableView.dataSource = self
        tableView.delegate = self
        var cellNib = UINib(nibName: IDS.searchCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: IDS.searchCell)
        
        cellNib = UINib(nibName: IDS.loadingCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: IDS.loadingCell)
        
        cellNib = UINib(nibName: IDS.notFoundCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: IDS.notFoundCell)
        
        //con la propiedad UIDevice podemos saber si estamos en un ipad o en un iphone, el nivel de la bateria, y demas cuestiones del device
        if UIDevice.current.userInterfaceIdiom != .pad {
            searchBar.becomeFirstResponder()
        }
    }

    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        performSearch()
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.willTransition(to: newCollection, with: coordinator)
        
        //El rect toma las medidas de la pantalla del dispositivo, si cumple con las medidas del iphone 7 Plus no mostramos el landscapeViewController
        let rect = UIScreen.main.bounds
        if (rect.width == 736 && rect.height == 414) || (rect.width == 414 && rect.height == 736) {
            if presentedViewController != nil {
                dismiss(animated: true, completion: nil)
            }
        }else if UIDevice.current.userInterfaceIdiom != .pad {
            switch newCollection.verticalSizeClass {
            case .compact:
                showLandscape(with: coordinator)
            case .regular, .unspecified:
                hideLandscape(with: coordinator)
            }
        }
    }
    
    fileprivate func performSearch() {
        if let category = Search.Category(rawValue: segmentedControl.selectedSegmentIndex) {
            search.performSearch(for: searchBar.text!, category: category, completion: {[unowned self] (success) in
                
                if !success {
                    self.showAlert(title: NSLocalizedString("There was a error", comment: "There was a error title"), message: NSLocalizedString("There was a error searching in the itunes server, try later.", comment: "There was a error message"))
                }
                self.tableView.reloadData()
                self.landscapeVC?.searchResultsReceived()
            })
            
            tableView.reloadData()
            searchBar.resignFirstResponder()
        }
    }
    
    func showAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(close)
        
        present(alert, animated: true, completion: nil)
    }
    
    //prepareSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDS.detailSegue {
            let vc = segue.destination as! DetailVC
            vc.isPopUp = true
            vc.searchResult = sender as! SearchResult
        }
    }
    
}

//MARK: -> SplitViewController
extension SearchVC {
    
    func hideMasterPane() {
        UIView.animate(
            withDuration: 0.25,
            animations: { [unowned self] in
                //la vista primaria o master sera ocultada
                self.splitViewController?.preferredDisplayMode = .primaryHidden
        }) { (_) in
            self.splitViewController?.preferredDisplayMode = .automatic
        }
    }
    
}

//MARK: -> UISearchBarDelegate
extension SearchVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        performSearch()
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}


//MARK: -> UITableViewDelegate and Datasource
extension SearchVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch search.state {
        case .noSearchedYet:
            return 0
        case .loading:
            return 1
        case .noResults:
            return 1
        case .results(let list):
            return list.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch search.state {
        case .noSearchedYet:
            fatalError("\n *** Should never get here *** \n")
        case .loading:
            let cell = tableView.dequeueReusableCell(withIdentifier: IDS.loadingCell, for: indexPath) as! LoadingCell
            cell.activityIndicator.startAnimating()
            
            return cell
        case .noResults:
            let cell = tableView.dequeueReusableCell(withIdentifier: IDS.notFoundCell, for: indexPath)
            return cell
        case .results(let searchResults):
            let cell = tableView.dequeueReusableCell(withIdentifier: IDS.searchCell, for: indexPath) as! SearchResultCell
            cell.updateUI(searchResult: searchResults[indexPath.row])
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        switch search.state {
        case .noResults, .noSearchedYet, .loading:
            return nil
        case .results(_):
            return indexPath
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if view.window?.rootViewController?.traitCollection.horizontalSizeClass == .compact {
            tableView.deselectRow(at: indexPath, animated: true)
            
            if case .results(let searchResults) = search.state {
                let searchResult = searchResults[indexPath.row]
                performSegue(withIdentifier: IDS.detailSegue, sender: searchResult)
            }
        }else {
            if case .results(let searchResults) = search.state {
                splitViewDetail?.searchResult = searchResults[indexPath.row]
                
                //el modo allVisible solo se aplica al modo landscape, de esta manera si estamos en portrait ocultamos el master al dar click a un resultado.
                if splitViewController?.displayMode != .allVisible {
                    hideMasterPane()
                }
            }
        }
    }
    
}

//MARK: -> show and hide landscapeVC
extension SearchVC {
    
    fileprivate func showLandscape(with coordinator: UIViewControllerTransitionCoordinator) {
        guard landscapeVC == nil else {
            return
        }
        
        landscapeVC = storyboard?.instantiateViewController(withIdentifier: IDS.landscapeVC) as? LandscapeVC
        
        if let controller = landscapeVC {
            controller.view.frame = view.bounds
            controller.view.alpha = 0
            controller.search = search
            
            view.addSubview(controller.view)
            self.addChildViewController(controller)
            
            coordinator.animate(alongsideTransition: {[unowned self] (_) in
                controller.view.alpha = 1
                self.searchBar.resignFirstResponder()
                
                //la propiedad presentedViewController nos indica si hay un viewController modal en pantalla
                if self.presentedViewController != nil {
                    self.dismiss(animated: true, completion: nil)
                }
                
            }, completion: { (_) in
                controller.didMove(toParentViewController: self)
            })
        }
    }
    
    fileprivate func hideLandscape(with coordinator: UIViewControllerTransitionCoordinator) {
        
        if let controller = landscapeVC {
            controller.willMove(toParentViewController: nil)
            coordinator.animate(alongsideTransition: {[unowned self] (_) in
                controller.view.alpha = 0
                if self.presentedViewController != nil {
                    self.dismiss(animated: true, completion: nil)
                }
            }, completion: {[unowned self] (_) in
                controller.view.removeFromSuperview()
                controller.removeFromParentViewController()
                self.landscapeVC = nil
            })
        }
        
    }
    
}


















//
//  DetailVC.swift
//  StoreSearch
//
//  Created by mazodirty on 2/06/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit
import MessageUI

class DetailVC: UIViewController {
    
    struct IDS {
        static let showMenu = "ShowMenu"
    }
    
    var searchResult: SearchResult! {
        didSet {
            if isViewLoaded {
                updateUI()
            }
        }
    }
    
    @IBOutlet weak var imageResult: UIImageView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var typeValueLabel: UILabel!
    @IBOutlet weak var genreValueLabel: UILabel!
    @IBOutlet weak var priceButton: UIButton!
    var downloadTask: URLSessionDownloadTask?
    
    //si es iphone sera un popup, si es ipad sera un viewController normal
    var isPopUp = false
    
    enum AnimationStyle {
        case slide
        case fade
    }
    
    var dismissAnimationStyle = AnimationStyle.fade
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popUpView.layer.cornerRadius = 10
        popUpView.backgroundColor = UIColor(white: 1.0, alpha: 0.95)
        
        if searchResult != nil {
            updateUI()
        }
        
        if isPopUp {
            view.backgroundColor = UIColor.clear
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(close))
            tapGesture.cancelsTouchesInView = false
            tapGesture.delegate = self
            view.addGestureRecognizer(tapGesture)
        }else {
            view.backgroundColor  = UIColor(patternImage: UIImage(named: "LandscapeBackground")!)
            popUpView.isHidden = true
            if let displayName = Bundle.main.localizedInfoDictionary?["CFBundleDisplayName"] as? String {
                //La propiedad title es usada por el navigationController para colocarle titulo al child controller actual
                title = displayName
            }
        }
        
    }
    
    //Este init es requerido para indicar que este viewController sera un delegate y tendra un UIPresentationController customizado
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        modalPresentationStyle = .custom
        transitioningDelegate = self
    }
    
    deinit {
        print("deinit \(self)")
        downloadTask?.cancel()
    }
    
    @IBAction func close() {
        dismissAnimationStyle = AnimationStyle.slide
        dismiss(animated: true, completion: nil)
    }
    
    
    fileprivate func updateUI() {
        nameLabel.text = searchResult.name
        if searchResult.artistName.isEmpty {
            artistName.text = NSLocalizedString("Unknown", comment: "dont know the artistName")
        }else {
            artistName.text = searchResult.artistName
        }
        typeValueLabel.text = searchResult.kindForDisplay()
        genreValueLabel.text = searchResult.genre
        if let url = URL(string: searchResult.artworkLargeURL) {
            downloadTask = imageResult.downLoadImage(url: url)
            imageResult.layer.cornerRadius = 7
        }
        
        //Formato moneda
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = searchResult.currency
        
        var currencyString: String
        if searchResult.price == 0 {
            currencyString = NSLocalizedString("Free", comment: "The price is 0")
        }else if let text = formatter.string(from: searchResult.price as NSNumber) {
            currencyString = text
        }else {
            currencyString = ""
        }
        
        priceButton.setTitle(currencyString, for: .normal)
        
        popUpView.isHidden = false
        popUpView.alpha = 0
        UIView.animate(
            withDuration: 0.4,
            animations: { [unowned self] in
                self.popUpView.alpha = 1
        },
            completion: nil
        )
    }
    
    @IBAction func openInStore(_ sender: UIButton) {
        if let url = URL(string: searchResult.storeURL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    //Prepare segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDS.showMenu {
            let vc = segue.destination as! MenuPopUpVC
            vc.delegate = self
        }
    }
    
}

//MARK: UIGestureRecognizerDelegate
extension DetailVC: UIGestureRecognizerDelegate {
    
    //se leera el touch si este coincide con el view principal(background negro) si no el touch se ignora
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        return (touch.view === self.view)
    }
}

//MARK: -> implementacion de mi custom UIPresentationController y custom animationController encargado de animar las transiciones
extension DetailVC: UIViewControllerTransitioningDelegate {
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        return DimmingPresentationController(presentedViewController: presented, presenting: presenting)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return BounceAnimationController()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch dismissAnimationStyle {
        case .slide:
            return SlideOutAnimationController()
        case .fade:
            return FadeOutAnimationController()
        }
    }
    
}

//MARK -> MenuPopUpVCDelegate
extension DetailVC: MenuPopUpVCDelegate {
    
    func MenuPopUpVCSendEmail(_ controller: MenuPopUpVC) {
        
        dismiss(animated: true) { 
            if MFMailComposeViewController.canSendMail() {
                let controller = MFMailComposeViewController()
                controller.setSubject(NSLocalizedString("Support Request", comment: "Email subject"))
                controller.setToRecipients(["mazodirty@gmail.com"])
                controller.mailComposeDelegate = self
                
                //La manera en que se presentara el mailCompose
                controller.modalPresentationStyle = .formSheet
                
                self.present(controller, animated: true, completion: nil)
            }
        }
        
    }
}


//MARK: -> Delegate de MFMailComposeViewController
extension DetailVC: MFMailComposeViewControllerDelegate {
    
    //Este metodo se ejecuta al enviar o cancelar en envio tambien se ejecuta si el envio ha sido exitoso o ha ocurrido un error
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        dismiss(animated: true, completion: nil)
    }
    
}
















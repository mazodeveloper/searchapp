//
//  FadeOutAnimationController.swift
//  StoreSearch
//
//  Created by mazodirty on 3/06/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class FadeOutAnimationController: NSObject, UIViewControllerAnimatedTransitioning {

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        if let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) {
            
            let duration = transitionDuration(using: transitionContext)
            
            UIView.animate(
                withDuration: duration,
                animations: {
                    fromView.alpha = 0
            },
                completion: { (finished) in
                    transitionContext.completeTransition(finished)
            }
            )
        }
        
    }
    
}

//
//  MenuPopUpVC.swift
//  StoreSearch
//
//  Created by mazodirty on 8/06/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

protocol MenuPopUpVCDelegate: class {
    func MenuPopUpVCSendEmail(_ controller: MenuPopUpVC)
}

class MenuPopUpVC: UITableViewController {
    
    @IBOutlet weak var emailCell: UITableViewCell!
    @IBOutlet weak var rateCell: UITableViewCell!
    @IBOutlet weak var aboutCell: UITableViewCell!
    
    weak var delegate: MenuPopUpVCDelegate?
    
    struct IDS {
        static let emailCell = "EmailCell"
        static let rateCell = "RateCell"
        static let aboutCell = "AboutCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backgroundView = UIView(frame: CGRect.zero)
        backgroundView.backgroundColor = UIColor(red: 20/255, green: 160/255, blue: 160/255, alpha: 0.5)
        
        emailCell.textLabel?.text = "Send Support Email"
        emailCell.selectedBackgroundView = backgroundView
        
        rateCell.textLabel?.text = "Rate this App"
        rateCell.selectedBackgroundView = backgroundView
        
        aboutCell.textLabel?.text = "About"
        aboutCell.selectedBackgroundView = backgroundView
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            delegate?.MenuPopUpVCSendEmail(self)
        }
    }
    
    
}















//
//  UIImageView+downloadImage.swift
//  StoreSearch
//
//  Created by mazodirty on 2/06/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

extension UIImageView {

    func downLoadImage(url: URL) -> URLSessionDownloadTask {
        
        let downloadTask = URLSession.shared.downloadTask(with: url) { (url, response, error) in
            
            if error == nil, let url = url, let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                DispatchQueue.main.async { [unowned self] in
                    self.image = image
                }
            }
        }
        
        downloadTask.resume()
        
        return downloadTask
    }

}

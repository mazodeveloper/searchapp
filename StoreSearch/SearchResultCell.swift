//
//  SearchResultCell.swift
//  StoreSearch
//
//  Created by mazodirty on 2/06/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {

    @IBOutlet weak var imageResult: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    var downloadTask: URLSessionDownloadTask?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let selectedView = UIView(frame: CGRect.zero)
        selectedView.backgroundColor = UIColor(red: 20/255, green: 160/255, blue: 160/255, alpha: 0.5)
        
        selectedBackgroundView = selectedView
    }
    
    func updateUI(searchResult: SearchResult) {
        nameLabel.text = searchResult.name
        if searchResult.artistName.isEmpty {
            artistNameLabel.text = NSLocalizedString("unknown", comment: "dont know the artistName")
        }else {
            artistNameLabel.text = String(format: NSLocalizedString("%@, (%@)", comment: "Format the artist name and kind"), searchResult.artistName, searchResult.kindForDisplay())
        }
        
        //DownloadImage
        imageResult.image = UIImage(named: "Placeholder")
        if let url = URL(string: searchResult.artworkSmallURL) {
            downloadTask = imageResult.downLoadImage(url: url)
        }
    }
    
    //este metodo se ejecuta cada vez que una celda es reutilizada por la tableView, en esta ocacion cancelaremos la descarga de la imagen si aun se encuentra en proceso
    override func prepareForReuse() {
        super.prepareForReuse()
        downloadTask?.cancel()
        downloadTask = nil
    }
    
}














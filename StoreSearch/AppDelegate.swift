//
//  AppDelegate.swift
//  StoreSearch
//
//  Created by mazodirty on 6/1/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    //Split viewController
    var splitViewController: UISplitViewController {
        return window?.rootViewController as! UISplitViewController
    }
    
    var searchController: SearchVC {
        return splitViewController.viewControllers.first as! SearchVC
    }
    
    var detailNavigationController: UINavigationController {
        return splitViewController.viewControllers.last as! UINavigationController
    }
    
    var detailViewController: DetailVC {
        return detailNavigationController.topViewController as! DetailVC
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        customUI()
        //Show display master button in navigation Detail
        detailViewController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
        searchController.splitViewDetail = detailViewController
        
        splitViewController.delegate = self
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    private func customUI() {
        UISearchBar.appearance().barTintColor = UIColor(red: 20.0/255.0, green: 160.0/255.0, blue: 160.0/255.0, alpha: 1.0)
        window?.tintColor = UIColor(red: 10.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0)
    }
    
    
}

//si el master se muestra dismiss los demas viewController presentes
//MARK: -> UISplitViewControllerDelegate 
extension AppDelegate: UISplitViewControllerDelegate {
    
    func splitViewController(_ svc: UISplitViewController, willChangeTo displayMode: UISplitViewControllerDisplayMode) {
        //este print se usa en debugging para ver que metodo se esta ejecutando
        print(#function)
        //si el master se va a mostrar cerraremos los demas modal y presents viewController
        if displayMode == .primaryOverlay {
            svc.dismiss(animated: true, completion: nil)
        }
    }
    
}













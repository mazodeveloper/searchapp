//
//  LoadingCell.swift
//  StoreSearch
//
//  Created by mazodirty on 2/06/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import Foundation
import UIKit

class LoadingCell: UITableViewCell {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

//
//  LandscapeVC.swift
//  StoreSearch
//
//  Created by mazodirty on 3/06/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class LandscapeVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    var search: Search!
    var firstTime = true
    var downloadTasks = [URLSessionDownloadTask]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.removeConstraints(view.constraints)
        view.translatesAutoresizingMaskIntoConstraints = true
        
        scrollView.removeConstraints(scrollView.constraints)
        scrollView.translatesAutoresizingMaskIntoConstraints = true
        scrollView.backgroundColor = UIColor(patternImage: UIImage(named: "LandscapeBackground")!)
        scrollView.delegate = self
        
        pageControl.removeConstraints(pageControl.constraints)
        pageControl.translatesAutoresizingMaskIntoConstraints = true
        pageControl.numberOfPages = 0
    }
    
    //Se ejecuta en el momento que la vista padre le dara a las vistas hijas el layout
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        scrollView.frame = view.bounds
        
        pageControl.frame = CGRect(
            x: 0,
            y: view.bounds.size.height - pageControl.bounds.height,
            width: view.bounds.width,
            height: pageControl.bounds.height
        )
        
        if firstTime {
            firstTime = false
            switch search.state {
            case .loading:
                showSpinner()
            case .noResults:
                showNothingFound()
            case .noSearchedYet:
                break
            case .results(let searchResults):
                tileButtons(searchResults)
            }
        }
    }
    
    private func showSpinner() {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinner.center = CGPoint(
            x: scrollView.bounds.midX + 0.5,
            y: scrollView.bounds.midY + 0.5
        )
        spinner.tag = 1000
        view.addSubview(spinner)
        spinner.startAnimating()
    }
    
    private func showNothingFound() {
        let notFoundLabel = UILabel(frame: CGRect.zero)
        notFoundLabel.text = NSLocalizedString("Nothing Found", comment: "Nothing Found")
        notFoundLabel.font = UIFont(name: "Avenir Next Condensed", size: 22)
        notFoundLabel.textColor = UIColor.white
        notFoundLabel.sizeToFit()
        
        var rect = notFoundLabel.frame
        //Lo que hace el objeto ceil es redondear cifras ejemplo:
        //si dividimos 11 / 2 el resultado sera 5.5 con ceil el resultado es 6
        rect.size.width = ceil(rect.size.width / 2) * 2
        rect.size.height = ceil(rect.size.height / 2) * 2
        
        notFoundLabel.frame = rect
        
        notFoundLabel.center = CGPoint(
            x: scrollView.bounds.midX,
            y: scrollView.bounds.midY
        )
        
        view.addSubview(notFoundLabel)
    }
    
    func searchResultsReceived() {
        hideSpinner()
        
        switch search.state {
        case .loading, .noSearchedYet:
            break
        case .noResults:
            showNothingFound()
        case .results(let searchResults):
            tileButtons(searchResults)
        }
    }
    
    private func hideSpinner() {
        view.viewWithTag(1000)?.removeFromSuperview()
    }
    
    private func tileButtons(_ searchResults: [SearchResult]) {
        
        var columnsPerPage = 5
        var rowsPerPage = 3
        var itemWidth:CGFloat = 96
        var itemHeight:CGFloat = 88
        var marginX: CGFloat = 0
        var marginY: CGFloat = 20
        
        let scrollViewWidth = scrollView.bounds.size.width
        
        switch scrollViewWidth {
        case 568:
            columnsPerPage = 6
            itemWidth = 94
            marginX = 2
        case 667:
            columnsPerPage = 7
            itemWidth = 95
            itemHeight = 98
            marginX = 1
            marginY = 29
        case 736:
            columnsPerPage = 8
            rowsPerPage = 4
            itemWidth = 92
        default:
            break
        }
        
        let buttonWidth:CGFloat = 82
        let buttonHeight: CGFloat = 82
        let paddingHorizontal = (itemWidth - buttonWidth) / 2
        let paddingVertical = (itemHeight - buttonHeight)  / 2
        
        var row = 0
        var column = 0
        var x = marginX
        
        for (index, searchResult) in searchResults.enumerated() {
            
            let button = UIButton(type: .custom)
            button.setBackgroundImage(UIImage(named: "LandscapeButton"), for: .normal)
            
            button.frame = CGRect(
                x: x + paddingHorizontal,
                y: marginY + itemHeight * CGFloat(row) + paddingVertical,
                width: buttonWidth,
                height: buttonHeight
            )
            
            button.tag = 2000 + index
            button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
            
            downloadImage(for: searchResult, button: button)
            
            scrollView.addSubview(button)
            
            row += 1
            if row == rowsPerPage {
                row = 0; x += itemWidth; column += 1
                
                if column == columnsPerPage {
                    column = 0; x += marginX * 2
                }
            }
        }
        
        let buttonsByPage = columnsPerPage * rowsPerPage
        let numPages = 1 + (searchResults.count - 1) / buttonsByPage
        let widthScroll = scrollViewWidth * CGFloat(numPages)
        scrollView.contentSize = CGSize(width: widthScroll, height: scrollView.bounds.size.height)
        
        pageControl.numberOfPages = numPages
        pageControl.currentPage = 0
        
    }
    
    func buttonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowDetail", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetail" {
            if case .results(let searchResults) = search.state {
                let detailVC = segue.destination as! DetailVC
                detailVC.isPopUp = true
                detailVC.searchResult = searchResults[(sender as! UIButton).tag - 2000]
            }
        }
    }
    
    fileprivate func downloadImage(for searchResult: SearchResult, button: UIButton) {
        
        if let url = URL(string: searchResult.artworkSmallURL) {
            
            let downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: {[weak button] (url, response, error) in
                
                if error == nil, let url = url, let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        if let button = button {
                            button.setImage(image, for: .normal)
                        }
                    }
                }
            })
            
            downloadTask.resume()
            downloadTasks.append(downloadTask)
        }
        
    }
    
    @IBAction func pageChanged(_ sender: UIPageControl) {
        
        UIView.animate(
            withDuration: 0.3,
            delay: 0,
            options: [.curveEaseInOut],
            animations: { [unowned self] in
                self.scrollView.contentOffset = CGPoint(
                    x: self.scrollView.bounds.size.width * CGFloat(sender.currentPage),
                    y: 0
                )
        },
            completion: nil
        )
    }
    
    
    deinit {
        print("deinit Landscape: \(self)")
        for downloadTask in downloadTasks {
            downloadTask.cancel()
        }
    }
    
}

//MARK: -> UIScrollViewDelegate
extension LandscapeVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let width = scrollView.bounds.size.width
        let currentPage = Int((scrollView.contentOffset.x + width / 2) / width)
        pageControl.currentPage = currentPage
    }
    
}

















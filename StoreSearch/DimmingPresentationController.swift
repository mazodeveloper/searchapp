//
//  DimmingPresentationController.swift
//  StoreSearch
//
//  Created by mazodirty on 2/06/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class DimmingPresentationController: UIPresentationController {
    
    //indicamos si la vista anterior a la nueva vista entrante va a desaparecer o no, en este caso deseamos que la vista siga en pantalla
    override var shouldRemovePresentersView: Bool {
        return false
    }
    
    lazy var dimmingView = GradientView(frame: CGRect.zero)
    
    //Notifica que la presentacion va a iniciar
    override func presentationTransitionWillBegin() {
        dimmingView.frame = containerView!.bounds
        containerView?.insertSubview(dimmingView, at: 0)
        dimmingView.alpha = 0
        
        //el transitionCoordinator es el encargado de realizar la transicion o animacion de una presentation and dismiss
        //el presentedViewController es el controller que va a entrar en pantalla
        if let coordinator = presentedViewController.transitionCoordinator {
            coordinator.animate(
                alongsideTransition: { [unowned self] (_) in
                    self.dimmingView.alpha = 1
            },
                completion: nil)
        }
    }
    
    
    override func dismissalTransitionWillBegin() {
        if let coordinator = presentedViewController.transitionCoordinator {
            coordinator.animate(
                alongsideTransition: { (_) in
                    self.dimmingView.alpha = 0
            },
                completion: nil)
        }
    }
    
}













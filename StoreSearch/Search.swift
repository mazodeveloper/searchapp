//
//  Search.swift
//  StoreSearch
//
//  Created by mazodirty on 5/06/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import Foundation
import UIKit

typealias SearchComplete = (Bool) -> Void

class Search {
    
    private var dataTask: URLSessionDataTask?
    private(set) var state:State = .noSearchedYet
    
    enum Category: Int {
        case all = 0
        case music = 1
        case software = 2
        case ebooks = 3
        
        //enum puede tener metodos y propiedades computadas, las propiedades por valor no son permitidas en los enum
        var entityName: String {
            switch self {
            case .all: return ""
            case .music: return "musicTrack"
            case .software: return "software"
            case .ebooks: return "ebook"
            }
        }
    }
    
    enum State {
        case noSearchedYet
        case loading
        case noResults
        case results([SearchResult])
    }
    
    func performSearch(for text: String, category: Category, completion: @escaping SearchComplete) {
        if !text.isEmpty {
            dataTask?.cancel()
            
            state = .loading
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let url = iTunesURL(searchText: text, category: category)
            dataTask = URLSession.shared.dataTask(with: url, completionHandler: {[unowned self] (data, response, error) in
                
                self.state = .noSearchedYet
                var success = false
                
                if let error = error as NSError?, error.code == -999 {
                    return //Search was cancelled
                }
                
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data, let jsonDictionary = self.parse(data: jsonData) {
                    
                    var searchResults = self.parse(dictionary: jsonDictionary)
                    if searchResults.isEmpty {
                        self.state = .noResults
                    }else {
                        searchResults.sort(by: {$0.name.localizedStandardCompare($1.name) == .orderedAscending })
                        self.state = .results(searchResults)
                    }
                    
                    success = true
                }
                
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    completion(success)
                }
            })
            
            dataTask?.resume()
        }
    }
    
    private func iTunesURL(searchText: String, category: Category) -> URL {
        
        let entityName = category.entityName
        // averiguaremos el idioma y la region del usuario
        let locale = Locale.autoupdatingCurrent
        let language = locale.identifier
        let countryCode = locale.regionCode ?? "en_US"
        
        //Cambiamos los caracteres especiales que no son permitidos en una URL
        let searchTextWithoutSpecialCharacters = searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        let path = String(format: "https://itunes.apple.com/search?term=%@&limit=200&entity=%@&lang=%@&country=%@", searchTextWithoutSpecialCharacters, entityName, language, countryCode)
        let url = URL(string: path)
        
        print("\n*** url: \(url!) ***\n")
        return url!
    }
    
    //Parseamos Data a Dictionary
    private func parse(data: Data) -> Dictionary<String, Any>? {
        do {
            if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                
                guard let _ = dictionary["results"] as? [Any] else {
                    return nil
                }
                
                return dictionary
            }
            
            return nil
            
        }catch let error {
            print("There was a error parsing data to dictionary: \(error)")
            return nil
        }
    }
    
    //Parseamos Dictionary o JSON a un array de Objectos (SearchResult)
    private func parse(dictionary: [String: Any]) -> [SearchResult] {
        
        guard let results = dictionary["results"] as? [Any] else {
            return []
        }
        
        var searchResults = [SearchResult]()
        
        for result in results {
            if let result = result as? [String: Any] {
                
                var searchResult: SearchResult?
                
                if let wrapperType = result["wrapperType"] as? String {
                    switch wrapperType {
                    case "track":
                        searchResult = parse(track: result)
                    case "audiobook":
                        searchResult = parse(audiobook: result)
                    case "software":
                        searchResult = parse(software: result)
                    default:
                        break
                    }
                }else if let kind = result["kind"] as? String, kind == "ebook" {
                    searchResult = parse(ebook: result)
                }
                
                if let tmp = searchResult {
                    searchResults.append(tmp)
                }
            }
        }
        
        return searchResults
    }
    
    //Parseamos json tipo track a SearchResult
    private func parse(track dictionary: [String: Any]) -> SearchResult {
        
        let searchResult = SearchResult()
        searchResult.name = dictionary["trackName"] as! String
        searchResult.artistName = dictionary["artistName"] as! String
        searchResult.artworkSmallURL = dictionary["artworkUrl60"] as! String
        searchResult.artworkLargeURL = dictionary["artworkUrl100"] as! String
        searchResult.storeURL = dictionary["trackViewUrl"] as! String
        searchResult.kind = dictionary["kind"] as! String
        searchResult.currency = dictionary["currency"] as! String
        
        if let price = dictionary["trackPrice"] as? Double {
            searchResult.price = price
        }
        
        if let genre = dictionary["primaryGenreName"] as? String {
            searchResult.genre = genre
        }
        
        return searchResult
    }
    
    //parse Json tipo audiobook a SearchResult
    private func parse(audiobook dictionary: [String: Any]) -> SearchResult {
        
        let searchResult = SearchResult()
        
        searchResult.name = dictionary["collectionName"] as! String
        searchResult.artistName = dictionary["artistName"] as! String
        searchResult.artworkSmallURL = dictionary["artworkUrl60"] as! String
        searchResult.artworkLargeURL = dictionary["artworkUrl100"] as! String
        searchResult.storeURL = dictionary["collectionViewUrl"] as! String
        searchResult.kind = "audiobook"
        
        searchResult.currency = dictionary["currency"] as! String
        if let price = dictionary["collectionPrice"] as? Double {
            searchResult.price = price
        }
        if let genre = dictionary["primaryGenreName"] as? String {
            searchResult.genre = genre
        }
        
        return searchResult
    }
    
    private func parse(software dictionary: [String: Any]) -> SearchResult {
        
        let searchResult = SearchResult()
        
        searchResult.name = dictionary["trackName"] as! String
        searchResult.artistName = dictionary["artistName"] as! String
        searchResult.artworkSmallURL = dictionary["artworkUrl60"] as! String
        searchResult.artworkLargeURL = dictionary["artworkUrl100"] as! String
        searchResult.storeURL = dictionary["trackViewUrl"] as! String
        searchResult.kind = dictionary["kind"] as! String
        searchResult.currency = dictionary["currency"] as! String
        
        if let price = dictionary["price"] as? Double {
            searchResult.price = price
        }
        if let genre = dictionary["primaryGenreName"] as? String {
            searchResult.genre = genre
        }
        
        return searchResult
    }
    
    private func parse(ebook dictionary: [String: Any]) -> SearchResult {
        
        let searchResult = SearchResult()
        
        searchResult.name = dictionary["trackName"] as! String
        searchResult.artistName = dictionary["artistName"] as! String
        searchResult.artworkSmallURL = dictionary["artworkUrl60"] as! String
        searchResult.artworkLargeURL = dictionary["artworkUrl100"] as! String
        searchResult.storeURL = dictionary["trackViewUrl"] as! String
        searchResult.kind = dictionary["kind"] as! String
        searchResult.currency = dictionary["currency"] as! String
        
        if let price = dictionary["price"] as? Double {
            searchResult.price = price
        }
        if let genres: Any = dictionary["genres"] {
            searchResult.genre = (genres as! [String]).joined(separator: ", ")
        }
        
        return searchResult
    }
    
}




















